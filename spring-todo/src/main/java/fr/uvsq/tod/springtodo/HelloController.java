/*package fr.uvsq.tod.springtodo;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	//private static GreetingRepository repository;
	private static long id = 0;
	
    @Autowired 
	static private GreetingRepository repository;
    
	private static ArrayList<Greeting> greetings = new ArrayList<Greeting>();
	
    @RequestMapping(value = "/HTTP",method = RequestMethod.GET)
    public ArrayList<Greeting> index() {
    	if(greetings.isEmpty())	{
    	/*for (Greeting greeting : repository.findAll()) {
			//greetings.add(greeting);
		}
    	}
    	greetings.add(new Greeting(1,"lol"));
		return greetings;
	}
    
    @RequestMapping(path = "/HTTP",method = RequestMethod.POST)
    public void newGreeting(@RequestParam(value = "name")String name) {
        repository.save(new Greeting(id++, name));
    }
    
}*/

package fr.uvsq.tod.springtodo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Classe de Controller qui va gerer les requettes.
 * @author Fouassier Edouard et Navarro Remi
 *
 */
@RestController
public class HelloController {
	private static long id = 0;
    private static ArrayList<Greeting> greetings = new ArrayList<Greeting>();
	
    
    /**
     * Methode GET avec le path "/HTTP" qui retourne la liste des taches 
     * et la remplie grace au fichier de sauvegarde si necessaire.
     * @return greetings la liste de tache
     * @throws IOException exception
     */
    @RequestMapping(value = "/HTTP",method = RequestMethod.GET)
    public ArrayList<Greeting> index() throws IOException {

    	if (greetings.isEmpty()) {
    		File f = new File("save.txt");
			BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
        	String line = buffer.readLine();
        	while (line != null) {
        		greetings.add(new Greeting(id++,line));
        		line = buffer.readLine();
        		}
        	buffer.close();
    	}
    	
		return greetings;
    	
    }
    
    /**
     * methode POST avec le path "/HTTP" qui ajoute une tache a la liste
     * et l'ajoute a la sauvegarde.
     * @param name le nom de la tache(content)
     * @throws IOException exception
     */
    @RequestMapping(path = "/HTTP",method = RequestMethod.POST)
    public void newGreeting(@RequestParam(value = "name")String name) throws IOException {
    	FileWriter out = new FileWriter("save.txt",true);
    	 out.write("\n" + name);
    	 greetings.add(new Greeting(id++,name));
    	 out.close();
    }
    
}


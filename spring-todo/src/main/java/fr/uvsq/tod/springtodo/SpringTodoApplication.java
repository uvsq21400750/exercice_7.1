package fr.uvsq.tod.springtodo;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
* Classe principale qui lance l'application.
* @author Fouassier Edouard et Navarro Remi
*
*/
@SpringBootApplication
@RestController
public class SpringTodoApplication {
	
	//private static final Logger log = LoggerFactory.getLogger(SpringTodoApplication.class);
	
	@RequestMapping("/")
	public String home() {
		return "Hello Docker World \n";
	}
	/**
	 * Classe main qui lance l'application.
	 * @param args arg d'entree
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringTodoApplication.class);
	}
	
	  /**
	  * bean permattant une persistence de l'application.
	  * @param ctx ApplicationContext
	  * @return args
	  */
	  @Bean
	    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
	        return args -> {

	            System.out.println("Let's inspect the beans provided by Spring Boot:");

	            String[] beanNames = ctx.getBeanDefinitionNames();
	            Arrays.sort(beanNames);
	            for (String beanName : beanNames) {
	                System.out.println(beanName);
	            }
	        };
	    }
}


package fr.uvsq.tod.springtodo;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Classe de tache avec un long id et un string content.
 * @author Fouassier Edouard et Navarro Remi
 *
 */
@Entity
public class Greeting {
	@Id
    private final long id;
    private final String content;
    
    public Greeting() {
        this.id = 0;
        this.content = "defaut";
    }
    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    @Override
    public String toString() {
    	
    	return String.format("Greeting[id=%d, content='%s']",id,content);
    }
}

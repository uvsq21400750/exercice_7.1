package fr.uvsq.tod.springtodo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
/**
 * Non utilisé car nous utilisons finalement un fichier texte.
 * @author user
 *
 */
public interface GreetingRepository extends CrudRepository<Greeting, Long> {

    List<Greeting> findByContent(String content);
}

